package com.example.matrimony.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matrimony.R;
import com.example.matrimony.activity.AddCandidateActivity;
import com.example.matrimony.activity.CandidateDetailsActivity;
import com.example.matrimony.adapter.UserListAdapter;
import com.example.matrimony.database.tables.UserTbl;
import com.example.matrimony.model.UserModel;
import com.example.matrimony.util.Constant;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserListFragment extends Fragment {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    UserListAdapter userListAdapter;
    ArrayList<UserModel> userList = new ArrayList<>();
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;

    MyFavoriteReceiver receiver = new MyFavoriteReceiver();

    public static UserListFragment getInstance(int gender) {

        UserListFragment fragment = new UserListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.GENDER, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_list, null);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);
        setAdapter();
        getActivity().registerReceiver(receiver,new IntentFilter(Constant.FAVORITE_CHANGE_FILTER));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,new IntentFilter(Constant.FAVORITE_CHANGE_FILTER));
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are You Sure Want To Delete");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedId = new UserTbl(getActivity()).deletUserById(userList.get(position).getUserId());

                    if (deletedId > 0) {
                        Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        userListAdapter.notifyItemRemoved(position);
                        userListAdapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }


    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userList.addAll(new UserTbl(getActivity()).getUserListByGender(getArguments().getInt(Constant.GENDER)));
        userListAdapter = new UserListAdapter(getActivity(), userList, new UserListAdapter.OnClickListener() {
            @Override
            public void onDeleteButtonClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), CandidateDetailsActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);
            }

            @Override
            public void onFavoriteButton(int position) {
                int lastUpdatedUserId = new UserTbl(getActivity()).updateFavoriteStatus(userList.get(position).
                        getIsFavorite() == 0 ? 1 : 0, userList.get(position).getUserId());

                if (lastUpdatedUserId > 0) {
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    userListAdapter.notifyItemChanged(position);
                }
            }
        });

        rcvUserList.setAdapter(userListAdapter);
        checkAndVisibleView();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    void openAddUserScreen() {
        Intent addUserIntent = new Intent(getActivity(), AddCandidateActivity.class);
        startActivity(addUserIntent);
    }

    @OnClick(R.id.btnAddUser)
    public void onViewClicked() {
        openAddUserScreen();
    }

    class MyFavoriteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(Constant.USER_ID)){
                int userId = intent.getIntExtra(Constant.USER_ID,0);
                checkUserIdAndUpdateStatus(userId);
            }
        }
    }

    void checkUserIdAndUpdateStatus(int userId){
        for (int i = 0; i < userList.size(); i++) {
            if (userId == userList.get(i).getUserId()){
                int isFavorite = userList.get(i).getIsFavorite();
                userList.get(i).setIsFavorite(isFavorite == 0 ? 1 : 0);
                userListAdapter.notifyItemChanged(i);
                return;
            }
        }
    }
}