package com.example.matrimony.database.tables;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.database.MyDatabase;
import com.example.matrimony.model.UserCityModel;

import java.util.ArrayList;

public class UserCityTbl extends MyDatabase {

    public static final String TABLE_NAME = "UserCityTbl";
    public static final String USER_CITY_ID = "UserCityId";
    public static final String NAME = "Name";

    public UserCityTbl(Context context) {
        super(context);
    }

    public ArrayList<UserCityModel> getCityList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserCityModel> cityList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i=0;i<cursor.getCount();i++)
        {
            UserCityModel userCityList = new UserCityModel();

            userCityList.setUserCityId(cursor.getInt(cursor.getColumnIndex(USER_CITY_ID)));
            userCityList.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            cityList.add(userCityList);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();

        return cityList;
    }
}
